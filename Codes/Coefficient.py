from __future__ import print_function

from os import listdir
from os.path import isfile, join
import os
import numpy
import math



K=10
baseList=[2,3,4,5,6]
InputFile='/home/karmake2/PracticalNDCG/Data/base.shuffled'
ResultDirectory='/home/karmake2/PracticalNDCG/Results/Coefficient/'
methods=['adaRank','ListNet','RankBoost','RandomForest','RankNet','CoordinateAscent','L2LR','LambdaMART']


try:
    os.remove(ResultDirectory+'summary.txt')
except:
    print('No summary file found')
    

Relevance={}           
with open(InputFile, 'r') as inputfile:
    for line in inputfile:
        relevance=float(line.split()[0])
        query=line.split()[1].split(':')[1]
        if query not in Relevance:
            docCounter=0
            Relevance[query]={}
        Relevance[query][docCounter]=relevance
        docCounter+=1



Score={}
for method in methods:
    Score[method]={}
    for fold in [1,2,3,4,5]:
        with open(ResultDirectory+'f'+str(fold)+'.'+method+'.txt','r') as inputfile:
            for line in inputfile:
                if line.strip()=='':
                    break
                query=line.split()[0]
                if query not in Score[method]:
                    Score[method][query]={}
                doc=int(line.split()[1])
                score=float(line.strip().split()[2])
                Score[method][query][doc]=score


NDCG={}
for base in baseList:
    if base not in NDCG:
        NDCG[base]={}
    for method in Score:
        if method not in NDCG[base]:
            NDCG[base][method]={}
        for query in Score[method]:
            DCG=0
            docList=sorted(Score[method][query],key=Score[method][query].get, reverse=True)[:K]
            for i in range(1,min(K+1,len(docList)+1)):
                DCG+=(math.pow(2,Relevance[query][docList[i-1]])-1)/(math.log(i+base-1))
                #DCG+=(Relevance[query][docList[i-1]])/(math.log(i+base-1))

            IDCG=0
            docList=sorted(Relevance[query],key=Relevance[query].get, reverse=True)[:K]
            for i in range(1,min(K+1,len(docList)+1)):
                IDCG+=(math.pow(2,Relevance[query][docList[i-1]])-1)/(math.log(i+base-1))
                #IDCG+=(Relevance[query][docList[i-1]])/(math.log(i+base-1))

            try:
                NDCG[base][method][query]=DCG/IDCG
            except:
                NDCG[base][method][query]=0.0



print('method', end='')
for base in baseList:
    print(','+str(base), end='')
print()



for method in methods:
    print(method, end='')
    for base in baseList:
        print(','+str(sum(NDCG[base][method].values())/len(NDCG[base][method])), end='')
    print()
        


QuerySpecificNDCG={}

for query in NDCG[baseList[0]]['adaRank']:
    QuerySpecificNDCG[query]={}
    for base in baseList:
        QuerySpecificNDCG[query][base]={}
        for method in methods:
            QuerySpecificNDCG[query][base][method]=NDCG[base][method][query]


QuerySpecificNDCGRank={}

for query in NDCG[baseList[0]]['adaRank']:
    QuerySpecificNDCGRank[query]={}
    for base in baseList:
        QuerySpecificNDCGRank[query][base]={}
        rank=1
        for method in sorted(QuerySpecificNDCG[query][base].items(), key=lambda x: (x[1],x[0]), reverse=True):
            QuerySpecificNDCGRank[query][base][method[0]]=rank
            rank+=1
        


QuerySpecificNDCGRankList={}

for query in NDCG[baseList[0]]['adaRank']:
    QuerySpecificNDCGRankList[query]={}
    for method in methods:
        QuerySpecificNDCGRankList[query][method]=[]
        for base in baseList:
            QuerySpecificNDCGRankList[query][method].append(QuerySpecificNDCGRank[query][base][method])





RankChanges={}
Variance={}
BestPerformerSwitch=[]

for query in QuerySpecificNDCGRankList:
    for method in methods:
        if len(set(QuerySpecificNDCGRankList[query][method]))>1:
            if query not in RankChanges:
                RankChanges[query]=[]
            RankChanges[query].append(method)

        if method not in Variance:
            Variance[method]=0
        Variance[method]+=(numpy.std(QuerySpecificNDCGRankList[query][method]))

        if QuerySpecificNDCGRankList[query][method][1]==1 and len(set(QuerySpecificNDCGRankList[query][method]))>1:
            BestPerformerSwitch.append(query)


TotalChanges=0
RankChangesMehodWise={}
for query in RankChanges:
    #print(query+': '+str(RankChanges[query]))
    TotalChanges+=len(RankChanges[query])
    for method in RankChanges[query]:
        if method in RankChangesMehodWise:
            RankChangesMehodWise[method]+=1
        else:
            RankChangesMehodWise[method]=1


print('\n\nTotal Number of Changes: '+str(TotalChanges))
print('\n\nNumber of Queries with at least one change: '+str(len(RankChanges)))

print('\nMethod Rank switch counts\n--------------------')
for method in sorted(RankChangesMehodWise,key=RankChangesMehodWise.get,reverse=True):
    print(method+': '+str(RankChangesMehodWise[method]))

print('\n\nMethod Rank Variance\n--------------------')
for method in sorted(Variance,key=Variance.get,reverse=True):
    print(method+': '+str(Variance[method]/1001))

    
print('\n\nNumber of Times the best performer is switched beyond K=10: '+str(len(BestPerformerSwitch)))

BestPerformerSwitchDistribution={}

for query in QuerySpecificNDCGRankList:
    for method in methods:
        if method not in BestPerformerSwitchDistribution:
            BestPerformerSwitchDistribution[method]=numpy.zeros(5)
        for i in range(len(QuerySpecificNDCGRankList[query][method])):
            if QuerySpecificNDCGRankList[query][method][i]==1:
                BestPerformerSwitchDistribution[method][i]+=1


print('\n\nBest Performer Distribution for different methods:\n-----------------------------')

for method in BestPerformerSwitchDistribution:
    mystr=method
    for j in range(len(BestPerformerSwitchDistribution[method])):
        mystr+=','+str(int(BestPerformerSwitchDistribution[method][j]))
    print(mystr)



