from os import listdir
from os.path import isfile, join
import os


ResultDirectory='/home/karmake2/PracticalNDCG/Results/VariableTestK/'
os.remove(ResultDirectory+'summary.txt')


onlyfiles = [f for f in listdir(ResultDirectory) if isfile(join(ResultDirectory, f))]

NDCG={}

for myfile in onlyfiles:
    method=myfile.replace('.txt','')
    NDCG[method]={}
    with open(ResultDirectory+myfile,'r') as inputfile:
        for line in inputfile:
            if 'NDCG@' in line and 'Test metric:' not in line:
                K=int(line.split()[0].split('@')[1])
                if K not in NDCG[method]:
                    NDCG[method][K]=[]
                NDCG[method][K].append(float(line.split(':')[1]))


with open(ResultDirectory+'summary.txt','w') as outputfile:
    outputfile.write('method\t')
    for K in sorted(NDCG['adaRank']):
        outputfile.write('NDCG@'+str(K)+'\t')
    outputfile.write('\n')

    for method in NDCG:
        outputfile.write(method+'\t')
        for K in sorted(NDCG[method]):
            outputfile.write(str(sum(NDCG[method][K])/len(NDCG[method][K]))+'\t')
        outputfile.write('\n')
