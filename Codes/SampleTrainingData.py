import random
TrainingSize=1000

TrainingFile='/srv/data/karmake2/MSR_LETOR/Fold1/train.txt'
OutputDirectory='/home/karmake2/PracticalNDCG/Data'

with open(TrainingFile, 'r') as inputfile:
    with open(OutputDirectory+'/base.data','w') as outputfile:
        QD={}
        while True:
            line=inputfile.readline()
            if line.strip()=='':
                break
            queryID=line.split()[1].split(':')[1]
            if queryID not in QD:
                QD[queryID]=[]
            QD[queryID].append(line)
        
        SampledQueries=sorted(random.sample(QD.keys(),1000))
        
        doc=0
        for q in SampledQueries:
            doc+=len(QD[q])
            for docs in QD[q]:
                outputfile.write(docs.strip()+'\n')
        doc=doc/len(SampledQueries)

        print('Training Data:  Query: '+str(len(QD))+', Doc: '+str(doc))
