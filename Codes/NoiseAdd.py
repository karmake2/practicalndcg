from os import listdir
from os.path import isfile, join
import os
import numpy


ResultDirectory='/home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/'
methods=['adaRank','L2LR','ListNet','RankBoost','CoordinateAscent','LambdaMART','RandomForest','RankNet']
K=[5,10,15,20,30]


try:
    os.remove(ResultDirectory+'summary.txt')
except:
    print('No summary file found')
    



NDCG={}

for K in [5,10,15,20,30]:
    NDCG[K]={}
    for method in methods:
        NDCG[K][method]={}
        for fold in [1,2,3,4,5]:
            with open(ResultDirectory+str(K)+'/f'+str(fold)+'.'+method+'.txt','r') as inputfile:
                for line in inputfile:
                    if line.strip()=='':
                        break
                    query=line.split()[1]
                    if query=='all':
                        continue
                    score=float(line.strip().split()[2])
                    NDCG[K][method][query]=score


QuerySpecificNDCG={}

for query in NDCG[5]['adaRank']:
    QuerySpecificNDCG[query]={}
    for K in [5,10,15,20,30]:
        QuerySpecificNDCG[query][K]={}
        for method in methods:
            QuerySpecificNDCG[query][K][method]=NDCG[K][method][query]


QuerySpecificNDCGRank={}

for query in NDCG[5]['adaRank']:
    QuerySpecificNDCGRank[query]={}
    for K in [5,10,15,20,30]:
        QuerySpecificNDCGRank[query][K]={}
        rank=1
        for method in sorted(QuerySpecificNDCG[query][K].items(), key=lambda x: (x[1],x[0]), reverse=True):
            QuerySpecificNDCGRank[query][K][method[0]]=rank
            rank+=1




RankChange={}


K=10
for CurrentMethod in methods:
    RankChange[CurrentMethod]={}
    for noise in [0.0,0.005,0.01,0.015,0.02,0.025,0.03,-0.005,-0.01,-0.015,-0.02,-0.025,-0.03]:
        RankChange[CurrentMethod][noise]=0
        for query in NDCG[5]['adaRank']:
            DummyDict={}
            for method in QuerySpecificNDCG[query][K]:
                DummyDict[method]=QuerySpecificNDCG[query][K][method]
            DummyDict[method]+=noise

            NewRank={}
            
            rank=1
            for method in sorted(DummyDict.items(), key=lambda x: (x[1],x[0]), reverse=True):
                NewRank[method[0]]=rank
                rank+=1

            if QuerySpecificNDCGRank[query][K][CurrentMethod]!=NewRank[CurrentMethod]:
                RankChange[CurrentMethod][noise]+=1

mystr='method'          
for noise in sorted(RankChange['adaRank']):
    mystr+=','+str(noise)
print(mystr)

for method in RankChange:
    mystr=method
    for noise in sorted(RankChange[method]):
        mystr+=','+str(RankChange[method][noise])
    print(mystr)











