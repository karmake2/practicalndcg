from os import listdir
from os.path import isfile, join
import os
import numpy


ResultDirectory='/home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/'
methods=['adaRank','L2LR','ListNet','RankBoost','CoordinateAscent','LambdaMART','RandomForest','RankNet']
K=[5,10,15,20,30]


try:
    os.remove(ResultDirectory+'summary.txt')
except:
    print('No summary file found')
    



NDCG={}

for K in [5,10,15,20,30]:
    NDCG[K]={}
    for method in methods:
        NDCG[K][method]={}
        for fold in [1,2,3,4,5]:
            with open(ResultDirectory+str(K)+'/f'+str(fold)+'.'+method+'.txt','r') as inputfile:
                for line in inputfile:
                    if line.strip()=='':
                        break
                    query=line.split()[1]
                    if query=='all':
                        continue
                    score=float(line.strip().split()[2])
                    NDCG[K][method][query]=score


QuerySpecificNDCG={}

for query in NDCG[5]['adaRank']:
    QuerySpecificNDCG[query]={}
    for K in [5,10,15,20,30]:
        QuerySpecificNDCG[query][K]={}
        for method in methods:
            QuerySpecificNDCG[query][K][method]=NDCG[K][method][query]


QuerySpecificNDCGRank={}

for query in NDCG[5]['adaRank']:
    QuerySpecificNDCGRank[query]={}
    for K in [5,10,15,20,30]:
        QuerySpecificNDCGRank[query][K]={}
        rank=1
        for method in sorted(QuerySpecificNDCG[query][K].items(), key=lambda x: (x[1],x[0]), reverse=True):
            QuerySpecificNDCGRank[query][K][method[0]]=rank
            rank+=1



QuerySpecificNDCGRankList={}

for query in NDCG[5]['adaRank']:
    QuerySpecificNDCGRankList[query]={}
    for method in methods:
        QuerySpecificNDCGRankList[query][method]=[]
        for K in [5,10,15,20,30]:
            QuerySpecificNDCGRankList[query][method].append(QuerySpecificNDCGRank[query][K][method])



RankChanges={}
Variance={}
BestPerformerSwitch=[]

for query in QuerySpecificNDCGRankList:
    for method in methods:
        if len(set(QuerySpecificNDCGRankList[query][method]))>1:
            if query not in RankChanges:
                RankChanges[query]=[]
            RankChanges[query].append(method)

        if method not in Variance:
            Variance[method]=0
        Variance[method]+=(numpy.std(QuerySpecificNDCGRankList[query][method]))

        if QuerySpecificNDCGRankList[query][method][1]==1 and len(set(QuerySpecificNDCGRankList[query][method][1:]))>1:
            BestPerformerSwitch.append(query)


TotalChanges=0
RankChangesMehodWise={}
for query in RankChanges:
    #print(query+': '+str(RankChanges[query]))
    TotalChanges+=len(RankChanges[query])
    for method in RankChanges[query]:
        if method in RankChangesMehodWise:
            RankChangesMehodWise[method]+=1
        else:
            RankChangesMehodWise[method]=1
            
    
print('\n\nTotal Number of Changes: '+str(TotalChanges))
print('\n\nNumber of Queries with at least one change: '+str(len(RankChanges)))

print('\nMethod Rank switch counts\n--------------------')
for method in sorted(RankChangesMehodWise,key=RankChangesMehodWise.get,reverse=True):
    print(method+' : '+str(RankChangesMehodWise[method])+' : '+str(Variance[method]/1000))
    
print('\n\nNumber of Times the best performer is switched beyond K=10: '+str(len(BestPerformerSwitch)))

BestPerformerSwitchDistribution={}

for query in QuerySpecificNDCGRankList:
    for method in methods:
        if method not in BestPerformerSwitchDistribution:
            BestPerformerSwitchDistribution[method]=numpy.zeros(5)
        for i in range(len(QuerySpecificNDCGRankList[query][method])):
            if QuerySpecificNDCGRankList[query][method][i]==1:
                BestPerformerSwitchDistribution[method][i]+=1


print('\n\nBest Performer Distribution for different methods:\n-----------------------------')

for method in BestPerformerSwitchDistribution:
    mystr=method
    for j in range(len(BestPerformerSwitchDistribution[method])):
        mystr+=','+str(int(BestPerformerSwitchDistribution[method][j]))
    print(mystr)
    
        



        
