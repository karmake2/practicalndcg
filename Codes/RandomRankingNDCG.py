import math

InputFile='/home/karmake2/PracticalNDCG/Data/base.shuffled'
OutputFile='/home/karmake2/PracticalNDCG/Results/RandomNDCG/random.txt'
ResultDirectory='/home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/'
methods=['adaRank','L2LR','ListNet','RankBoost','CoordinateAscent','LambdaMART','RandomForest','RankNet']

Relevance={}
RelevanceList={}

with open(InputFile, 'r') as inputfile:
    for line in inputfile:
        relevance=float(line.split()[0])
        query=line.split()[1].split(':')[1]
        if query not in Relevance:
            Relevance[query]={}
        if relevance in Relevance[query]:
            Relevance[query][relevance]+=1
        else:
            Relevance[query][relevance]=1
        if query not in RelevanceList:
            RelevanceList[query]=[]
        RelevanceList[query].append(relevance)


ExpectedRList={}
for query in Relevance:
    docCount=0
    ExpectedR=0
    for r in Relevance[query]:
        docCount+=Relevance[query][r]
    for r in Relevance[query]:
        ExpectedR+=r*(Relevance[query][r]/float(docCount))
    ExpectedRList[query]=ExpectedR
        

toolimitedDoc=0

RandomNDCG={}
with open(OutputFile,'w') as outputfile:
    for query in Relevance:
        if query not in RandomNDCG:
            RandomNDCG[query]={}
        if len(RelevanceList[query])<10:
            toolimitedDoc+=1
        
        outputfile.write(str(query)+':')
        for K in [5,10,15,20,30]:
            IdealRankList=sorted(RelevanceList[query],reverse=True)[0:K] 
            DCG=0
            for i in range(1,min(K+1,len(IdealRankList)+1)):
                DCG+=ExpectedRList[query]*(1/math.log(i+1,2))
                
            
            IDCG=0
            for i in range(1,min(K+1,len(IdealRankList)+1)):
                IDCG+=IdealRankList[i-1]*(1/math.log(i+1,2))
            try:
                NDCG=DCG/IDCG
            except:
                NDCG=0.0
            RandomNDCG[query][K]=NDCG
            outputfile.write(str(NDCG))
            if K<30:
                outputfile.write(',')
        outputfile.write('\n')
                
print(toolimitedDoc)           


ActualNDCG={}
AdjustedNDCG={}

for K in [5,10,15,20,30]:
    AdjustedNDCG[K]={}
    ActualNDCG[K]={}
    for method in methods:
        AdjustedNDCG[K][method]={}
        ActualNDCG[K][method]={}
        for fold in [1,2,3,4,5]:
            with open(ResultDirectory+str(K)+'/f'+str(fold)+'.'+method+'.txt','r') as inputfile:
                for line in inputfile:
                    if line.strip()=='':
                        break
                    query=line.split()[1]
                    score=float(line.strip().split()[2])
                    ActualNDCG[K][method][query]=score
                    try:
                        AdjustedNDCG[K][method][query]=score*(float(RandomNDCG[query][K])/(score+RandomNDCG[query][K]))
                    except:
                        AdjustedNDCG[K][method][query]=0.0


for method in methods:
    mystr=method
    for K in [5,10,15,20,30]:
        mystr+=','+str(sum(AdjustedNDCG[K][method].values())/len(AdjustedNDCG[K][method]))
    print(mystr)




QueryLevelRRStatistics={}
for K in [5,10,15,20,30]:
    QueryLevelRRStatistics[K]={}
    for query in ActualNDCG[K]['adaRank']:
        normalizedDict={}
        StandardDict={}
        normalizedDictRank={}
        StandardDictRank={}
        for method in ActualNDCG[K]:
            StandardDict[method]=ActualNDCG[K][method][query]
            normalizedDict[method]=AdjustedNDCG[K][method][query]

        rank=1
        for method in sorted(StandardDict, key=StandardDict.get, reverse=True):
            StandardDictRank[method]=rank
            rank+=1

        rank=1
        for method in sorted(normalizedDict, key=normalizedDict.get, reverse=True):
            normalizedDictRank[method]=rank
            rank+=1

        if query=='all':
            continue
        for method in ActualNDCG[K]:
            if method not in QueryLevelRRStatistics[K]:
                QueryLevelRRStatistics[K][method]=0
            if normalizedDictRank[method]!=StandardDictRank[method]:
                QueryLevelRRStatistics[K][method]+=1

        '''print(query)
        print(normalizedDictRank)
        print(StandardDictRank)
        print(StandardDict)
        print(normalizedDict)'''
        
        

    
for method in QueryLevelRRStatistics[5]:
    mystr=method
    for K in [5,10,15,20,30]:
        mystr+=','+str(QueryLevelRRStatistics[K][method])
    print(mystr)
    
            
            
            





        

            
            
