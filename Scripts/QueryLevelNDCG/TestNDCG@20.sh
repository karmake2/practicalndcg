#NDCG@20

#adarank tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/adaRank/f1.adaRank -test /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f1.adaRank.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/adaRank/f2.adaRank -test /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f2.adaRank.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/adaRank/f3.adaRank -test /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f3.adaRank.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/adaRank/f4.adaRank -test /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f4.adaRank.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/adaRank/f5.adaRank -test /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f5.adaRank.txt








#L2LR tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/L2LR/f1.L2LR -test /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f1.L2LR.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/L2LR/f2.L2LR -test /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f2.L2LR.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/L2LR/f3.L2LR -test /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f3.L2LR.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/L2LR/f4.L2LR -test /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f4.L2LR.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/L2LR/f5.L2LR -test /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f5.L2LR.txt








#ListNet tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/ListNet/f1.ListNet -test /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f1.ListNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/ListNet/f2.ListNet -test /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f2.ListNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/ListNet/f3.ListNet -test /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f3.ListNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/ListNet/f4.ListNet -test /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f4.ListNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/ListNet/f5.ListNet -test /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f5.ListNet.txt









#RankBoost tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankBoost/f1.RankBoost -test /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f1.RankBoost.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankBoost/f2.RankBoost -test /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f2.RankBoost.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankBoost/f3.RankBoost -test /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f3.RankBoost.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankBoost/f4.RankBoost -test /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f4.RankBoost.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankBoost/f5.RankBoost -test /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f5.RankBoost.txt








#CoordinateAscent tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/CoordinateAscent/f1.CoordinateAscent -test /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f1.CoordinateAscent.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/CoordinateAscent/f2.CoordinateAscent -test /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f2.CoordinateAscent.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/CoordinateAscent/f3.CoordinateAscent -test /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f3.CoordinateAscent.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/CoordinateAscent/f4.CoordinateAscent -test /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f4.CoordinateAscent.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/CoordinateAscent/f5.CoordinateAscent -test /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f5.CoordinateAscent.txt











#LambdaMART tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/LambdaMART/f1.LambdaMART -test /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f1.LambdaMART.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/LambdaMART/f2.LambdaMART -test /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f2.LambdaMART.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/LambdaMART/f3.LambdaMART -test /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f3.LambdaMART.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/LambdaMART/f4.LambdaMART -test /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f4.LambdaMART.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/LambdaMART/f5.LambdaMART -test /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f5.LambdaMART.txt











#RandomForest tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RandomForest/f1.RandomForest -test /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f1.RandomForest.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RandomForest/f2.RandomForest -test /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f2.RandomForest.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RandomForest/f3.RandomForest -test /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f3.RandomForest.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RandomForest/f4.RandomForest -test /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f4.RandomForest.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RandomForest/f5.RandomForest -test /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f5.RandomForest.txt












#RankNet tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankNet/f1.RankNet -test /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f1.RankNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankNet/f2.RankNet -test /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f2.RankNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankNet/f3.RankNet -test /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f3.RankNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankNet/f4.RankNet -test /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f4.RankNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankNet/f5.RankNet -test /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -metric2T NDCG@20 -idv /home/karmake2/PracticalNDCG/Results/QueryLevelNDCG/20/f5.RankNet.txt










