cd /home/karmake2/PracticalNDCG/Models/TrainedFixedK

mkdir RankNet adaRank L2LR LambdaMART RankBoost RandomForest CoordinateAscent ListNet

cd /home/karmake2/PracticalNDCG/Scripts/TrainingScripts

chmod 755 adaRank.Train.sh
./adaRank.Train.sh

chmod 755 L2LR.Train.sh
./L2LR.Train.sh

chmod 755 LambdaMART.Train.sh
./LambdaMART.Train.sh

chmod 755 RankBoost.Train.sh
./RankBoost.Train.sh

chmod 755 RF.Train.sh
./RandomForest.Train.sh

chmod 755 RankNet.Train.sh
./RankNet.Train.sh

chmod 755 CA.Train.sh
./CoordinateAscent.Train.sh

chmod 755 LN.Train.sh
./ListNet.Train.sh

