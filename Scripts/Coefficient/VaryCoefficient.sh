#adarank tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/adaRank/f1.adaRank -rank /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f1.adaRank.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/adaRank/f2.adaRank -rank /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f2.adaRank.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/adaRank/f3.adaRank -rank /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f3.adaRank.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/adaRank/f4.adaRank -rank /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f4.adaRank.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/adaRank/f5.adaRank -rank /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f5.adaRank.txt








#L2LR tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/L2LR/f1.L2LR -rank /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f1.L2LR.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/L2LR/f2.L2LR -rank /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f2.L2LR.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/L2LR/f3.L2LR -rank /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f3.L2LR.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/L2LR/f4.L2LR -rank /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f4.L2LR.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/L2LR/f5.L2LR -rank /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f5.L2LR.txt








#ListNet tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/ListNet/f1.ListNet -rank /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f1.ListNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/ListNet/f2.ListNet -rank /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f2.ListNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/ListNet/f3.ListNet -rank /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f3.ListNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/ListNet/f4.ListNet -rank /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f4.ListNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/ListNet/f5.ListNet -rank /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f5.ListNet.txt









#RankBoost tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankBoost/f1.RankBoost -rank /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f1.RankBoost.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankBoost/f2.RankBoost -rank /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f2.RankBoost.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankBoost/f3.RankBoost -rank /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f3.RankBoost.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankBoost/f4.RankBoost -rank /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f4.RankBoost.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankBoost/f5.RankBoost -rank /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f5.RankBoost.txt








#CoordinateAscent tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/CoordinateAscent/f1.CoordinateAscent -rank /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f1.CoordinateAscent.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/CoordinateAscent/f2.CoordinateAscent -rank /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f2.CoordinateAscent.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/CoordinateAscent/f3.CoordinateAscent -rank /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f3.CoordinateAscent.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/CoordinateAscent/f4.CoordinateAscent -rank /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f4.CoordinateAscent.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/CoordinateAscent/f5.CoordinateAscent -rank /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f5.CoordinateAscent.txt











#LambdaMART tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/LambdaMART/f1.LambdaMART -rank /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f1.LambdaMART.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/LambdaMART/f2.LambdaMART -rank /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f2.LambdaMART.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/LambdaMART/f3.LambdaMART -rank /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f3.LambdaMART.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/LambdaMART/f4.LambdaMART -rank /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f4.LambdaMART.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/LambdaMART/f5.LambdaMART -rank /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f5.LambdaMART.txt











#RandomForest tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RandomForest/f1.RandomForest -rank /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f1.RandomForest.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RandomForest/f2.RandomForest -rank /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f2.RandomForest.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RandomForest/f3.RandomForest -rank /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f3.RandomForest.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RandomForest/f4.RandomForest -rank /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f4.RandomForest.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RandomForest/f5.RandomForest -rank /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f5.RandomForest.txt












#RankNet tests
java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankNet/f1.RankNet -rank /home/karmake2/PracticalNDCG/Data/Folds/f1.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f1.RankNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankNet/f2.RankNet -rank /home/karmake2/PracticalNDCG/Data/Folds/f2.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f2.RankNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankNet/f3.RankNet -rank /home/karmake2/PracticalNDCG/Data/Folds/f3.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f3.RankNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankNet/f4.RankNet -rank /home/karmake2/PracticalNDCG/Data/Folds/f4.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f4.RankNet.txt

java -jar /home/karmake2/PracticalNDCG/RankLib/RankLib.jar -load /home/karmake2/PracticalNDCG/Models/TrainedFixedK/RankNet/f5.RankNet -rank /home/karmake2/PracticalNDCG/Data/Folds/f5.test.base.shuffled -score /home/karmake2/PracticalNDCG/Results/Coefficient/f5.RankNet.txt










